import unittest
from main import function, angle


class TestTDD(unittest.TestCase):
    def test_function(self):
        self.assertEqual(function('y=ax+b'), 'y=ax+b')

    def test_angle(self):
        self.assertEqual(angle(30), 30)
